<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
 
    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function get()
    {
        $posts = Post::where('user_id',auth()->user()->id)->get();
        return view('profile',['posts'=>$posts]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $fileUrl = null;
        if($request->hasFile('imgupload')){
            $path = $request->imgupload->store('imgupload',['disk'=>'public']);

            $fileUrl = Storage::url($path);
        }

        if($fileUrl === null and trim($request->post_text) == ""){
            return redirect()->back();
        }

        $user = auth()->user();
        $user->profile_pic = $fileUrl;
        $user->save();


        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();
        return redirect('/loggedin/home');
    }
}
