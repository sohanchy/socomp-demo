<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::latest()->limit(20)->with(['likes','author'])->get();

        return view('home', [
            'posts' => $posts
        ]);
    }

    public function create(Request $request)
    {
        return view('create', [
        ]);
    }

    public function menu(Request $request)
    {
        return view('menu', [
        ]);
    }

    public function post(Request $request)
    {
        $fileUrl = null;
        if($request->hasFile('imgupload')){
            $path = $request->imgupload->store('imgupload',['disk'=>'public']);

            $fileUrl = Storage::url($path);
        }

        if($fileUrl === null and trim($request->post_text) == ""){
            return redirect()->back();
        }

        $post = new Post();
        $post->content = trim($request->post_text);
        $post->photo = $fileUrl;
        $post->user_id = auth()->user()->id;
        
        $post->save();
        return redirect('./');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }
}
