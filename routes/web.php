<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::middleware(['auth'])->prefix('/loggedin')->group(function () {
    Route::get('/home', [HomeController::class,'index'])->name('home');
    Route::get('/menu', [HomeController::class,'menu'])->name('menu');
    Route::get('/create', [HomeController::class,'create'])->name('create');
    Route::post('/create', [HomeController::class,'post'])->name('post');


    Route::resource('/post', PostController::class);
    Route::get('/post/{post}/like', [PostController::class,'like']);

    Route::get('/profile', [ProfileController::class,'get']);
    Route::post('/profile', [ProfileController::class,'update']);
});

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
