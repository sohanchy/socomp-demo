<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title>Facebook</title>

    <!-- Bootstrap core CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('shepherd.css')}}">

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="/docs/5.1/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="/docs/5.1/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="/docs/5.1/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="/docs/5.1/assets/img/favicons/manifest.json">
    <link rel="mask-icon" href="/docs/5.1/assets/img/favicons/safari-pinned-tab.svg" color="#7952b3">
    <link rel="icon" href="/docs/5.1/assets/img/favicons/favicon.ico">
    <meta name="theme-color" content="#7952b3">


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }
        
        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
        
        .fb-bgcolor {
            background-color: #4267B2;
        }
        
        .fbicon {
            background-image: url('{{url("icons.png")}}');
            background-repeat: no-repeat;
            background-size: 25px 1184px;
            background-position: 0 -470px;
            height: 20px;
            width: 20px;
        }
        
        .fbicon-row {
            padding-top: 10px;
            padding-bottom: 10px;
        }
        
        .twofivepix {
            height: 25px;
            width: 25px;
        }
    </style>


    <!-- Custom styles for this template -->
    <link href="navbar-top.css" rel="stylesheet">
    <link href="{{url('css/helpme.css')}}" rel="stylesheet">
</head>

<body>
    <button id="helpMeButton" style="border: none;" class="fab"> ? </button>
    <nav class="navbar navbar-dark mb-2 fb-bgcolor">
        <div class="container">
            <div class="col-1" style="background-image: url('./icons.png');
            background-repeat: no-repeat;
            background-size: 25px 1184px;
            background-position: 0 -890px;
            height: 20px;
            width: 20px;"></div>
            <div class="col-10 text-center text-white border-bottom">
                <i style="width: 16px;
                height: 16px;
                background-position: 0 -1012px;
                margin-right: 10px;
                background-image: url('./icons.png');
                background-size: 25px 1184px;
                background-repeat: no-repeat;
                display: inline-block;
                "></i> <span style="line-height: 20px;">Search</span>
            </div>
            <div class="col-1" style="background-image: url('./icons.png');
            background-repeat: no-repeat;
            background-size: 25px 1184px;
            background-position: 0 -197px;
            height: 20px;
            width: 20px;"></div>
        </div>
    </nav>
    <form method="POST" enctype="multipart/form-data">
    @csrf
    <div class="container-fluid">
        <div class="row m-2 fbicon-row text-center">
            <a class="col-2 fbicon" style="background-position: 0 0; display:block" href="{{url('./')}}"></a>
            <div class="col-8" style="text-align: left;">Create Post</div>
            <div class="col-2"></div>
            <div class="col-1 text-right" style="padding: right 30px;"><button id="postItFinal" type="submit" style="text-decoration: none; font-weight: bold; border: 0; background: 0;">Post</a></div>
        </div>

        <div class="row m-2 fbicon-row text-center" style="border-top: 0.5px solid #DADDE1; border-bottom: 1px solid #DADDE1;">
            <div class="col-2">
                <img class="rounded-circle" style="height: 40px; width: 40px; border: 1px solid #DADDE1;" src="{{url(auth()->user()->profile_pic)}}"></i>
            </div>
            <div class="col-8" style="text-align: left; font-size: 14px;">
                {{auth()->user()->name}} <br/> <span style="font-size: 12px;">🌎 Share with: Public </span>
            </div>

            <textarea name="post_text" id="writeHere" style="width: 100%; padding-top: 10px; border: 0; border-top: 1px solid #DADDE1;margin-top: 10px;" placeholder="What's on your mind?" rows=4></textarea>


            <div id="imgPreviewHolder" class="p-4;" style="border: 1px dashed; height: 80px; width: 80px; display: none;">
                <img id="imgPreview" style="width: 100%; height: 100%;;" />
            </div>

            <!-- <div class="p-4;" style="border: 1px dashed; height: 80px; width: 80px;">
                <img src="./res/PhotoPlus.png" style="width: 100%; margin-top: 10px;" />
            </div>
            <div class="p-4;" style="margin-left: 15px; border: 1px dashed; height: 80px; width: 80px;">
                <img src="./res/VideoPlus.png" style="width: 100%; margin-top: 10px;" />
            </div> -->
        </div>
    </div>

    <div class="container-fluid">


        <div id="photoUploadBtnRegion" class="row text-center" style="border-top: 1px solid #DADDE1; border-bottom: 1px solid #DADDE1;">
            <div class="col-1" style="margin-right: 0; padding-right: 0px;">
                <div class="col fbicon" style="background-position: 0 -92px; margin-top: 8px;"></div>
            </div>
            <div class="col py-1" style="text-align: left; line-height: 35px; padding-left: 0px;">
                Photo
            </div>
            <input type="file" id="imgupload" name="imgupload" style="display:none" />
        </div>


        <div class="row text-center" style="border-top: 1px solid #DADDE1; border-bottom: 1px solid #DADDE1;">
            <div class="col-1" style="margin-right: 0; padding-right: 0px;">
                <div class="col fbicon" style="background-position: 0 -137px; margin-top: 8px;"></div>
            </div>
            <div class="col py-1" style="text-align: left; line-height: 35px; padding-left: 0px;">
                Video
            </div>
        </div>
        <div class="row text-center" style="border-top: 1px solid #DADDE1; border-bottom: 1px solid #DADDE1;">
            <div class="col-1" style="margin-right: 0; padding-right: 0px;">
                <div class="col fbicon" style="background-position: 0 -71px; margin-top: 8px;"></div>
            </div>
            <div class="col py-1" style="text-align: left; line-height: 35px; padding-left: 0px;">
                Tag Friends
            </div>
        </div>
        <div class="row text-center" style="border-top: 1px solid #DADDE1; border-bottom: 1px solid #DADDE1;">
            <div class="col-1" style="margin-right: 0; padding-right: 0px;">
                <div class="col fbicon" style="background-position: 0 -113px; margin-top: 8px;"></div>
            </div>
            <div class="col py-1" style="text-align: left; line-height: 35px; padding-left: 0px;">
                Location
            </div>
        </div>
        <div class="row text-center" style="border-top: 1px solid #DADDE1; border-bottom: 1px solid #DADDE1;">
            <div class="col-1" style="margin-right: 0; padding-right: 0px;">
                <div class="col fbicon" style="background-position: 0 -50px; margin-top: 8px;"></div>
            </div>
            <div class="col py-1" style="text-align: left; line-height: 35px; padding-left: 0px;">
                Feeling / Activity
            </div>
        </div>
    </div>
    </form>

    <script src="{{url('shepherd_beautified.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js" integrity="sha384-skAcpIdS7UcVUC05LJ9Dxay8AXcDYfBJqt1CJ85S/CFujBsIzCIv+l9liuYLaMQ/" crossorigin="anonymous"></script>
    <script>
        function docReady(fn) {
            // see if DOM is already available
            if (document.readyState === "complete" || document.readyState === "interactive") {
                // call on next available tick
                setTimeout(fn, 1);
            } else {
                document.addEventListener("DOMContentLoaded", fn);
            }
        }

        docReady(function() {

            console.log("HEREREE");
            const tour = new Shepherd.Tour({
                useModalOverlay: true,
                defaultStepOptions: {
                    classes: 'rounded border-2',
                    scrollTo: true
                }
            });

            tour.addStep({
                id: 'wdywntdo',
                text: 'To start writing a status touch here.',
                attachTo: {
                    element: document.getElementById('writeHere'),
                    on: 'bottom'
                },
                classes: 'example-step-extra-class',
                buttons: [{
                    text: 'Done',
                    action: tour.complete
                }, {
                    text: "Next",
                    action: tour.next
                }]
            });

            tour.addStep({
                id: 'pwt',
                text: 'To upload a photo touch here.',
                attachTo: {
                    element: document.getElementById('photoUploadBtnRegion'),
                    on: 'bottom'
                },
                classes: 'example-step-extra-class',
                buttons: [{
                    text: 'Done',
                    action: tour.complete
                }, {
                    text: 'Next',
                    action: tour.next
                }, ]
            });


            tour.addStep({
                id: 'postSmth',
                text: 'To finally post it, click here.',
                attachTo: {
                    element: document.getElementById('postItFinal'),
                    on: 'bottom'
                },
                scrollTo: {
                    behavior: 'smooth',
                    block: 'center'
                },
                classes: 'example-step-extra-class',
                buttons: [{
                    text: 'Done',
                    action: tour.complete
                }]
            });

            const hmb = document.getElementById('helpMeButton');
            hmb.onclick = function() {
                tour.start();
            };
        });
    </script>

    <script>
        document.getElementById('photoUploadBtnRegion').onclick = function() {
            document.getElementById('imgupload').click();
        }

        const imgInp = document.getElementById('imgupload');
        imgInp.onchange = evt => {
            const [file] = imgInp.files
            if (file) {
                document.getElementById('imgPreview').src = URL.createObjectURL(file);
                document.getElementById('imgPreviewHolder').style.display = "inline-block";
            }
        }
    </script>
</body>

</html>