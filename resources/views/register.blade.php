<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>      <html class="no-js"> <!--<![endif]-->
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('shepherd.css')}}">


    <style>
        body {
            text-align: left;
            direction: ltr;
        }
        
        .login-input {
            font-family: Helvetica, Arial, sans-serif;
            font-size: 14px;
            background: #f5f6f7;
            padding: 12px;
        }
        
        .body-container {
            padding: 0 16px;
            margin: 0 auto;
            max-width: 416px;
            font-size: 14px;
            line-height: 18px;
        }
        
        .login-btn {
            font-size: 17px;
            height: 40px;
            display: block;
            width: 100%;
            border: none;
            border-radius: 3px;
            box-sizing: border-box;
            position: relative;
            -webkit-user-select: none;
            z-index: 0;
        }
    </style>

<link href="{{url('css/helpme.css')}}" rel="stylesheet">
</head>

<body>
<button id="helpMeButton" style="border: none;" class="fab"> ? </button>
    <!--[if lt IE 7]>
      <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <div style="width: 100%; background-color: #fffbe2; 
    font-family: Roboto, 'Droid Sans', Helvetica, sans-serif; font-size: 14px; padding-top: 4px;">
        <div class="container">
            <div class="row">
                <div class="col-1">
                    <i class="img l img _2sxw" style="background-image: url('res/mobile_icon.png');
                    display: inline-block;
                    margin: 2px 8px;
                    border: 0;
                    background-repeat:no-repeat;background-size:100% 100%;-webkit-background-size:100% 100%;width:18px;height:28px;"></i>
                </div>
                <div class="col-11 pt-2" style="color: #3b5998;" id="getFacebook">
                    <span class="fcl">Get Facebook for Android and browse faster.</span>
                </div>
            </div>
        </div>
    </div>

    <a href="{{url('/')}}">
    <img src="res/fb_logo.svg" class="mx-auto d-block mt-2" alt="FB LOGO" width="112">
    </a>

    <div class="body-container text-center">

    <!-- Session Status -->
    <x-auth-session-status class="mb-4" :status="session('status')" />

    <!-- Validation Errors -->
    <x-auth-validation-errors class="mb-4" :errors="$errors" />

    <form method="POST" action="{{ route('register') }}">
            @csrf

            <input name="fname" type="fname" class="form-control login-input" id="fname" placeholder="First Name">
            <div style="padding: 4px;"></div>
            <input name="lname" type="lname" class="form-control login-input" id="lname" placeholder="Last Name">
            <div style="padding: 4px;"></div>
            <input name="email" type="email" class="form-control login-input" id="email" placeholder="Email">
            <div style="padding: 4px;"></div>
            <input name="password" type="password" class="form-control login-input" id="pw" placeholder="Password">
            <div style="padding: 4px;"></div>
            <button id="loginbtn" type="submit" class="btn btn-success login-btn">Register</button>
            <div style="padding: 4px;"></div>
           
        </form>
    </div>


    <div style="padding: 20px;"></div>


    <div id="langsel" class="container text-center" style="
    font-size: 12px;
    line-height: 16px;
    
    color: #576b95;
cursor: pointer;
text-decoration: none;
">
        <div class="row">
            <div class="col-6" style="color: #90949c;font-weight: bold;">English (US)</div>
            <div class="col-6">Français (Canada)</div>
        </div>
        <div style="padding: 1px;"></div>
        <div class="row">
            <div class="col-6">Español</div>
            <div class="col-6">中文(简体)</div>
        </div>
        <div style="padding: 1px;"></div>
        <div class="row">
            <div class="col-6">Português (Brasil)</div>
            <div class="col-6">Deutsch</div>
        </div>
        <div style="padding: 1px;"></div>
        <div class="row">
            <div class="col-6">Italiano</div>
            <div class="col-6">
                <i class="img" style="background-image: url( 'res/plus.png'); background-size:  contain; background-repeat: no-repeat; display: inline-block; height: 20px; width: 20px;"></i>
            </div>
        </div>
    </div>
    </div>

    <div style="padding: 6px;"></div>

    <div class="text-center" style="color: #8a8d91;">

        <span style="display: inline-block;
        font-size: 10px;
        line-height: 13px;
        margin: 2px 3px;
        padding: 0 2px;
        position: relative;
        top: -2px;
        white-space: nowrap;
        word-break: keep-all;">About · Help · More</span>


        <br/>
        <span class="" style="display: inline-block;
    font-size: 12px;
    line-height: 13px;
    margin: 2px 3px;
    padding: 0 2px;
    position: relative;
    top: -2px;
    white-space: nowrap;
    word-break: keep-all;" class="mfss fcg">Facebook Inc.</span>

    </div>



    <script src="{{url('shepherd_beautified.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js" integrity="sha384-skAcpIdS7UcVUC05LJ9Dxay8AXcDYfBJqt1CJ85S/CFujBsIzCIv+l9liuYLaMQ/" crossorigin="anonymous"></script>
    <script>

        function docReady(fn) {
            // see if DOM is already available
            if (document.readyState === "complete" || document.readyState === "interactive") {
                // call on next available tick
                setTimeout(fn, 1);
            } else {
                document.addEventListener("DOMContentLoaded", fn);
            }
        }

        docReady(function() {

            const tour = new Shepherd.Tour({
                useModalOverlay: true,
                defaultStepOptions: {
                    classes: 'rounded border-2',
                    scrollTo: true
                }
            });

            tour.addStep({
                id: 'fname1',
                text: 'Type your first name here.',
                attachTo: {
                    element: document.getElementById('fname'),
                    on: 'bottom'
                },
                classes: 'example-step-extra-class',
                buttons: [{
                    text: 'Next',
                    action: tour.next
                }]
            });

            tour.addStep({
                id: 'lname2',
                text: 'And your last name here.',
                attachTo: {
                    element: document.getElementById('lname'),
                    on: 'bottom'
                },
                classes: 'example-step-extra-class',
                buttons: [{
                    text: 'Next',
                    action: tour.next
                }]
            });

            tour.addStep({
                id: 'email1',
                text: 'Write your email address here <brr/>(It looks something like: yourname@gmail.com)',
                attachTo: {
                    element: document.getElementById('email'),
                    on: 'bottom'
                },
                classes: 'example-step-extra-class',
                buttons: [{
                    text: 'Next',
                    action: tour.next
                }]
            });

            tour.addStep({
                id: 'pwd',
                text: 'Write a password here that you will remember.<br/> A password is some words and numbers that are secret and only known to you. <br/> For example: rabbit1997tiger',
                attachTo: {
                    element: document.getElementById('pw'),
                    on: 'bottom'
                },
                classes: 'example-step-extra-class',
                buttons: [{
                    text: 'Next',
                    action: tour.next
                }]
            });


            tour.addStep({
                id: 'example-step1111',
                text: 'Youre almost done, just touch here to complete creating your account.',
                attachTo: {
                    element: document.getElementById('loginbtn'),
                    on: 'bottom'
                },
                classes: 'example-step-extra-class',
                buttons: [{
                    text: 'Done',
                    action: tour.complete
                }]
            });



            const hmb = document.getElementById('helpMeButton');
            hmb.onclick = function() {
                tour.start();
            };;
        });
    </script>


</body>

</html>

</html>