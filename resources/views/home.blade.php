<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.88.1">
    <title>Facebook</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.1/examples/navbar-static/">



    <!-- Bootstrap core CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link rel="stylesheet" href="{{url('shepherd.css')}}">

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="/docs/5.1/assets/img/favicons/apple-touch-icon.png" sizes="180x180">
    <link rel="icon" href="/docs/5.1/assets/img/favicons/favicon-32x32.png" sizes="32x32" type="image/png">
    <link rel="icon" href="/docs/5.1/assets/img/favicons/favicon-16x16.png" sizes="16x16" type="image/png">
    <link rel="manifest" href="/docs/5.1/assets/img/favicons/manifest.json">
    <link rel="mask-icon" href="/docs/5.1/assets/img/favicons/safari-pinned-tab.svg" color="#7952b3">
    <link rel="icon" href="/docs/5.1/assets/img/favicons/favicon.ico">
    <meta name="theme-color" content="#7952b3">


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }
        
        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
        
        .fb-bgcolor {
            background-color: #4267B2;
        }
        
        .fbicon {
            background-image: url({{url('./icons.png')}});
            background-repeat: no-repeat;
            background-size: 25px 1184px;
            background-position: 0 -470px;
            height: 20px;
            width: 20px;
        }
        
        .fbicon-row {
            padding-top: 10px;
            padding-bottom: 10px;
        }
    </style>


    <!-- Custom styles for this template -->
    <link href="navbar-top.css" rel="stylesheet">
    <link href="{{url('css/helpme.css')}}" rel="stylesheet">
</head>

<body>
<button id="helpMeButton" style="border: none;" class="fab"> ? </button>
    <nav class="navbar navbar-dark mb-2 fb-bgcolor">
        <div class="container">
            <div class="col-1" style="background-image: url({{url('./icons.png')}});
            background-repeat: no-repeat;
            background-size: 25px 1184px;
            background-position: 0 -890px;
            height: 20px;
            width: 20px;"></div>
            <div class="col-10 text-center text-white border-bottom">
                <i style="width: 16px;
                height: 16px;
                background-position: 0 -1012px;
                margin-right: 10px;
                background-image: url({{url('./icons.png')}});
                background-size: 25px 1184px;
                background-repeat: no-repeat;
                display: inline-block;
                "></i> <span style="line-height: 20px;">Search</span>
            </div>
            <div class="col-1" style="background-image: url({{url('./icons.png')}});
            background-repeat: no-repeat;
            background-size: 25px 1184px;
            background-position: 0 -197px;
            height: 20px;
            width: 20px;"></div>
        </div>
    </nav>

    <div class="container-fluid" style="border-bottom: 1px solid #DADDE1;">
        <div class="row m-2 fbicon-row text-center mx-2" style="margin-right: -1.5rem!important;margin-left: 1.5rem!important;">
            <a class="col fbicon" style="background-position: 0 -743px; display:block" href="{{url('./')}}"></a>
            <div class="col fbicon" style="background-position: 0 -470px;"></div>
            <div class="col fbicon" style="background-position: 0 -638px;"></div>
            <div class="col fbicon" style="background-position: 0 -932px;"></div>
            <div class="col fbicon" style="background-position: 0 -323px;"></div>
            <a id="menuBtn" href="{{url('loggedin/menu')}}" class="col fbicon" style="display:block; background-position: 0 -722px;"></a>
        </div>
    </div>

    <div class="container-fluid" style="border-bottom: 1px solid #DADDE1;">
        <div class="row fbicon-row text-center">
            <div class="col-1">
                <img class="rounded-circle" style="height: 40px; width: 40px; border: 1px solid #DADDE1;  overflow:hidden; object-fit: cover;" src="{{url(auth()->user()->profile_pic)}}">
            </div>
            <div class="col-9 mx-2">
                <input id="onymind" type="text" class="form-control rounded-pill text-black" placeholder="What's on your mind?">
            </div>
            <div class="col-1" id="postPhoto" style="padding-right: 30px;">
                <a href="./create"><i class="fbicon mt-2" style="margin-right: 5px; background-position: 0 -25px; display: block; height: 25px; width: 25px;"></i></a>

            </div>
        </div>
    </div>

    @foreach($posts as $post)

    <div style="background-color: #DADDE1;" class="mt-2">
        <main id="feed">
            <div class="rounded px-2" style="background-color: white;">
                <div class="row">
                    <div class="col-1">
                        <img src="{{url($post->author->profile_pic)}}" class="rounded-circle" style="height: 40px; width: 40px; border: 1px solid #DADDE1; overflow:hidden; object-fit: cover;"></i>

                    </div>
                    <div class="col-11">
                        <h5 style="padding-left: 10px; font-size: 18px;line-height: 36px;">{{$post->author->name}}</h1>
                    </div>
                </div>

                {{ $post->content }} <br/>
                @if($post->photo)
                <img src="{{url($post->photo)}}" style="width: 98%; max-width: 450px; margin-left: 0%; margin-top: 2px;" />
                @endif
                </br>
                
                <a id="postDetails" style="color: black; text-decoration: none;" href="{{url('loggedin/post/'.$post->id)}}">
                {{count($post->likes)}} Likes, {{count($post->comments)}} Comments </a>
                <div class="row text-center mt-1 border-top pt-1">
                    <div class="col"><a href="{{url('loggedin/post/'.$post->id.'/like')}}" 
                    style="text-decoration: none; color: black;" class="col"> 👍 Like </a></div>
                    <a id="commentBtn" class="col" style="display: block;color: black; text-decoration: none;" href="{{url('loggedin/post/'.$post->id)}}"> 💬 Comment</a>
                    <div class="col"> 🔗 Share</div>
                </div>
    </div>

    @endforeach


    @for($i =0; $i < 20; $i++)
    <div style="background-color: #DADDE1;" class="mt-2">
        <main id="feed">
            <div class="rounded px-2" style="background-color: white;">
                <div class="row">
                    <div class="col-1">
                        <img src="{{url('./res/oldpropic.jpg')}}" class="rounded-circle" style="height: 40px; width: 40px; border: 1px solid #DADDE1;"></i>

                    </div>
                    <div class="col-11">
                        <h5 style="padding-left: 10px; font-size: 18px;line-height: 36px;">John Someone</h1>
                    </div>
                </div>

                <!-- <img src="{{url('./res/pic.jpg')}}" style="width: 98%; margin-left: 1%; margin-top: 2px;" /> -->
                <div class="row text-center mt-1 border-top pt-1">
                    <div class="col"> 👍 Like</div>
                    <div class="col"> 💬 Comment</div>
                    <div class="col"> 🔗 Share</div>
                </div>
    </div>
    @endfor



        </main>
    </div>


    <script src="{{url('shepherd_beautified.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.min.js" integrity="sha384-skAcpIdS7UcVUC05LJ9Dxay8AXcDYfBJqt1CJ85S/CFujBsIzCIv+l9liuYLaMQ/" crossorigin="anonymous"></script>
    <script>
        document.getElementById("onymind").onclick = function() {
            location.href = "./create";
        };

        function docReady(fn) {
            // see if DOM is already available
            if (document.readyState === "complete" || document.readyState === "interactive") {
                // call on next available tick
                setTimeout(fn, 1);
            } else {
                document.addEventListener("DOMContentLoaded", fn);
            }
        }

        docReady(function() {

            const tour = new Shepherd.Tour({
                useModalOverlay: true,
                defaultStepOptions: {
                    classes: 'rounded border-2',
                    scrollTo: true
                }
            });

            tour.addStep({
                id: 'wdywntdo',
                text: 'What do you want to do?',

                classes: 'example-step-extra-class',
                buttons: [{
                    text: "Post something",
                    action: () => {
                        return tour.show('postSmth')
                    }
                },
                {
                    text: 'Browse Posts',
                    action: tour.next
                }, 
                {
                    text: "Other Options",
                    action: () => {
                        return tour.show('otherOpt')
                    }
                }
            ]
            });

            tour.addStep({
                id: 'pwt',
                text: 'You just touch and drag up to scroll up and down.',
                attachTo: {
                    element: document.getElementById('feed'),
                    on: 'top'
                },
                classes: 'example-step-extra-class',
                buttons: [
                    {
                    text: 'How do I comment?',
                    action: () => {
                        return tour.show('commentHow')
                        }
                    },
                    {
                    text: 'I want to post something.',
                    action: () => {
                        return tour.show('postSmth')
                    }
                }, {
                    text: 'Done',
                    action: tour.complete
                }]
            });


            tour.addStep({
                id: 'postSmth',
                text: 'To post a status update touch here.',
                attachTo: {
                    element: document.getElementById('onymind'),
                    on: 'bottom'
                },
                scrollTo: {
                    behavior: 'smooth',
                    block: 'center'
                },
                classes: 'example-step-extra-class',
                buttons: [{
                    text: 'Post a Photo',
                    action: tour.next
                }, {
                    text: 'Done',
                    action: tour.complete
                }]
            });


            tour.addStep({
                id: 'photopw',
                text: 'To post a photo touch here.',
                attachTo: {
                    element: document.getElementById('postPhoto'),
                    on: 'bottom'
                },
                classes: 'example-step-extra-class',
                buttons: [{
                    text: 'Done',
                    action: tour.complete
                }]
            });

            tour.addStep({
                id: 'commentHow',
                text: 'To leave a comment, click on the comment button on any post ',
                attachTo: {
                    element: document.getElementById('commentBtn'),
                    on: 'bottom'
                },
                scrollTo: {
                    behavior: 'smooth',
                    block: 'center'
                },
                classes: 'example-step-extra-class',
                buttons: [{
                    text: 'Done',
                    action: tour.complete
                }]
            });

            tour.addStep({
                id: 'otherOpt',
                text: 'To access other options menu, such as your profile or changing profile picture, <br/> click here.',
                attachTo: {
                    element: document.getElementById('menuBtn'),
                    on: 'bottom'
                },
                scrollTo: {
                    behavior: 'smooth',
                    block: 'center'
                },
                classes: 'example-step-extra-class',
                buttons: [{
                    text: 'Done',
                    action: tour.complete
                }]
            });


            const hmb = document.getElementById('helpMeButton');
            hmb.onclick = function() {
                tour.start();
            };;
        });
    </script>



</body>

</html>